//soal 2
function cetakFunction() {
  const nama_kamu = "Tri Bagas Iskandar"; 
  
  return `Hallo Nama saya ${nama_kamu}`;
}

console.log(cetakFunction());

//soal 3
function myFunction(angka1, angka2) {
  const hasil = angka1 + angka2;
  return hasil.toString();
}

let angka1 = 20;
let angka2 = 7;
let output = myFunction(angka1, angka2);

console.log(output);
//soal 4
const Hello = () => {
  return "Hello";
}

console.log(Hello());
//soal 5
let obj = {
  nama: "john",
  umur: 22,
  bahasa: "indonesia"
}

console.log(obj.bahasa);

//soal 6
let arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku" , 1992];
let objDaftarPeserta = {
  nama: arrayDaftarPeserta[0],
  jenisKelamin: arrayDaftarPeserta[1],
  hobi: arrayDaftarPeserta[2],
  tahunLahir: arrayDaftarPeserta[3]
};
console.log(objDaftarPeserta);
//soal 7
let dataBuah = [
  {
    nama: "Nanas",
    warna: "Kuning",
    adaBijinya: false,
    harga: 9000
  },
  {
    nama: "Jeruk",
    warna: "Oranye",
    adaBijinya: true,
    harga: 8000
  },
  {
    nama: "Semangka",
    warna: "Hijau & Merah",
    adaBijinya: true,
    harga: 10000
  },
  {
    nama: "Pisang",
    warna: "Kuning",
    adaBijinya: false,
    harga: 5000
  }
];

let buahTanpaBiji = dataBuah.filter(buah => !buah.adaBijinya);
console.log(buahTanpaBiji);

//soal 8
let phone = {
  name: "Galaxy Note 20",
  brand: "Samsung",
  year: 2020
}

let { name, brand, year } = phone;

//soal 9
let dataBukuTambahan= {
  penulis: "john doe",
  tahunTerbit: 2020 
}

let buku = {
  nama: "pemograman dasar",
  jumlahHalaman: 172
}

let objOutput = { ...buku, ...dataBukuTambahan };

console.log(objOutput);

//soal 10
let mobil = {

merk : "bmw",

color: "red",

year : 2002

}

 

const functionObject = (param) => {

return param

}

console.log(functionObject(mobil))