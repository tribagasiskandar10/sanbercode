

for (let index = 0; index < 10; index++) {
    console.log(index); 
    
}

// soal 3
for (var i = 1; i <= 10; i += 2) {
    console.log(i);
  }

  //soal 4
  for (var i = 2; i <= 8; i += 2) {
    console.log(i);
  }
  //soal 5 
  let array1 = [1,2,3,4,5,6];
console.log(array1[5]);
// soal 5
let array2 = [5,2,4,1,3,5];
array2.sort();

console.log(array2);

//soal 6
let array3 = ["selamat", "anda", "melakukan", "perulangan", "array", "dengan", "for"]

for (let index = 0; index < array3.length; index++) {
    console.log(array3[index]); 
    
}

//soal 7
let array4 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

for (let i = 0; i < array4.length; i++) {
  if (array4[i] % 2 == 0) {
    console.log(array4[i]);
  }
}
//soal 8
let kalimat = ["saya", "sangat", "senang", "belajar", "javascript"];
let gabung = kalimat.join(" ");

console.log(gabung);

//soal 9
var sayuran = [];

sayuran.push("Kangkung");
sayuran.push("Bayam");
sayuran.push("Buncis");
sayuran.push("Kubis");
sayuran.push("Timun");
sayuran.push("Seledri");
sayuran.push("Tauge");

console.log(sayuran);